import { Injectable } from '@nestjs/common';
import { Model } from "mongoose";
import { InjectModel } from "@nestjs/mongoose";
import { user } from "./interfaces/user.interface";
import { CreateUserDTO } from "./dto/user.dto";
import { create } from 'domain';

@Injectable()
export class UserService {

    constructor(@InjectModel('user') private readonly userModel: Model<user> ) {}

        async getUsers(): Promise<user[]> {
            const users = await this.userModel.find();
            return users;
        }

        async getUser(userID: String): Promise<user> {
            const user = await this.userModel.findById(userID);
            return user;

        }

        async createUser(createUserDTO: CreateUserDTO): Promise<user> {
            const user =  new this.userModel(createUserDTO);
            return await user.save();
            //return user;

        }

        async deleteUser(userID: String): Promise<user> {
            const deletedUser = await this.userModel.findByIdAndDelete (userID);
            return deletedUser;

        }

        async updateUser(userID: String, createUserDTO: CreateUserDTO): Promise<user> {
            const updateUser = await this.userModel.findByIdAndUpdate(userID, createUserDTO, {new: true});
            return updateUser;
        }
        
    
}
