import { Module } from '@nestjs/common';
import { UserController } from './user.controller';
import { UserService } from './user.service';
import { MongooseModule, Schema } from "@nestjs/mongoose";
import { UsersSchema } from './schemas/user.schema';

@Module({
  imports: [
    MongooseModule.forFeature([
      {name: 'user', schema: UsersSchema} 
    ])
  ],
  controllers: [UserController],
  providers: [UserService],
})
export class UserModule {}
