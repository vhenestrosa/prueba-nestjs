import { Controller, Get, Post, Put, Delete, Res, HttpStatus, Body, Param, NotFoundException, Query } from '@nestjs/common';

import { CreateUserDTO } from "./dto/user.dto"; 
import { UserService } from "./user.service";

@Controller('user')
export class UserController {

  constructor(private userService: UserService) {}

  @Get('/')
  async getUsers(@Res() res ) {
    const users = await this.userService.getUsers(); 
    return res.status(HttpStatus.OK).json({
      message: 'all users ',
      users: users // = users
    });
  }

  @Get('/:userID')
  async getUser(@Res()res, @Param('userID') userID) {
    const userGet = await this.userService.getUser(userID); 
    if(!userGet) throw new NotFoundException('the user not exists');
    return res.status(HttpStatus.OK).json({
      message: 'user selected',
      userGet: userGet
    });
  }
  
  @Post('/create')
  async createUser(@Res() res, @Body() createUserDTO: CreateUserDTO) {
    const user = await this.userService.createUser(createUserDTO);
    return res.status(HttpStatus.OK).json({
      message: 'user create successful',
      user: user
    });
  }

  @Put('/update')
  async updateUser(@Res() res, @Query('userID') userID, @Body() createUserDTO: CreateUserDTO) {
    const updateUser = await this.userService.updateUser(userID, createUserDTO);
    if(!updateUser) throw new NotFoundException('the user not exists');
    return res.status(HttpStatus.OK).json({
      Message: 'user actualized',
      updateUser: updateUser});
  }
  
  @Delete('/delete')
  async deleteUser(@Res() res, @Query('userID') userID) {
    const userDeleted = await this.userService.deleteUser(userID);
    if(!userDeleted) throw new NotFoundException('the user not exists');
    return res.status(HttpStatus.OK).json({
      Message: 'user deleted',
      userDeleted: userDeleted});
  }

}
