export class CreateUserDTO {

    readonly name: String;
    readonly description: String;
    readonly imageURL: String;
    readonly nickname: String;
    readonly password: String;
    readonly createAt: Date;
}