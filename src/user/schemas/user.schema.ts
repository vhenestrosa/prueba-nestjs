import { Schema } from "mongoose"

export const UsersSchema = new Schema({
    name: {type: String, required: true},
    decryption: String,
    imageURL: String,
    nickname: String,
    password: String,
    createAt: {
        type: Date,
        default: Date.now
    }
});