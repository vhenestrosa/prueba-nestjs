import { Controller, Get, Post, Put, Delete, Res, HttpStatus, Body, Param, NotFoundException, Query } from '@nestjs/common';

import { CreateProductDTO } from "./dto/product.dto"; 
import { ProductService } from "./product.service";

@Controller('product')
export class ProductController {

  constructor(private productService: ProductService) {}

  @Get('/')
  async getProducts(@Res() res ) {
    const products = await this.productService.getProducts(); 
    return res.status(HttpStatus.OK).json({
      message: 'all products ',
      products: products // = products
    });
  }

  @Get('/:productID')
  async getProduct(@Res()res, @Param('productID') productID) {
    const productGet = await this.productService.getProduct(productID); 
    if(!productGet) throw new NotFoundException('the product not exists');
    return res.status(HttpStatus.OK).json({
      message: 'product selected',
      productGet: productGet
    });
  }
  
  @Post('/create')
  async createProduct(@Res() res, @Body() createProductDTO: CreateProductDTO) {
    const product = await this.productService.createProduct(createProductDTO);
    return res.status(HttpStatus.OK).json({
      message: 'product create successful',
      product: product
    });
  }

  @Put('/update')
  async updateProduct(@Res() res, @Query('productID') productID, @Body() createProductDTO: CreateProductDTO) {
    const updateProduct = await this.productService.updateProduct(productID, createProductDTO);
    if(!updateProduct) throw new NotFoundException('the product not exists');
    return res.status(HttpStatus.OK).json({
      Message: 'product actualized',
      updateProduct: updateProduct});
  }
  
  @Delete('/delete')
  async deleteProduct(@Res() res, @Query('productID') productID) {
    const productDeleted = await this.productService.deleteProduct(productID);
    if(!productDeleted) throw new NotFoundException('the product not exists');
    return res.status(HttpStatus.OK).json({
      Message: 'product deleted',
      productDeleted: productDeleted});
  }

}
