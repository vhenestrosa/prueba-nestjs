import { Injectable } from '@nestjs/common';
import { Model } from "mongoose";
import { InjectModel } from "@nestjs/mongoose";
import { product } from "./interfaces/product.interface";
import { CreateProductDTO } from "./dto/product.dto";
import { create } from 'domain';

@Injectable()
export class ProductService {

    constructor(@InjectModel('product') private readonly productModel: Model<product> ) {}

        async getProducts(): Promise<product[]> {
            const products = await this.productModel.find();
            return products;
        }

        async getProduct(productID: String): Promise<product> {
            const product = await this.productModel.findById(productID);
            return product;

        }

        async createProduct(createProductDTO: CreateProductDTO): Promise<product> {
            const product =  new this.productModel(createProductDTO);
            return await product.save();
            //return product;

        }

        async deleteProduct(productID: String): Promise<product> {
            const deletedProduct = await this.productModel.findByIdAndDelete (productID);
            return deletedProduct;

        }

        async updateProduct(productID: String, createProductDTO: CreateProductDTO): Promise<product> {
            const updateProduct = await this.productModel.findByIdAndUpdate(productID, createProductDTO, {new: true});
            return updateProduct;
        }
        
    
}
