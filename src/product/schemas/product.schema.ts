import { Schema } from "mongoose"

export const ProductsSchema = new Schema({
    name: {type: String, required: true},
    decryption: String,
    imageURL: String,
    price: Number,
    createAt: {
        type: Date,
        default: Date.now
    }
});