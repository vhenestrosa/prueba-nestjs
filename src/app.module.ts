import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { ShopModule } from './shop/shop.module';
import { ProductModule } from "./product/product.module";
import { UserModule } from "./user/user.module";
import { CartModule } from './cart/cart.module';
import { MongooseModule } from "@nestjs/mongoose";

@Module({
  imports: [
    ShopModule, MongooseModule.forRoot('mongodb://localhost/shop'),
    ProductModule, MongooseModule.forRoot('mongodb://localhost/shop'),
    UserModule, MongooseModule.forRoot('mongodb://localhost/shop'),
    CartModule, MongooseModule.forRoot('mongodb://localhost/shop'),
  
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
