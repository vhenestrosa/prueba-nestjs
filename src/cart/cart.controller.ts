import { Controller, Get, Post, Put, Delete, Res, HttpStatus, Body, Param, NotFoundException, Query } from '@nestjs/common';

import { CreateCartDTO } from "./dto/cart.dto"; 
import { CartService } from "./cart.service";

@Controller('cart')
export class CartController {

  constructor(private cartService: CartService) {}

  @Get('/')
  async getCarts(@Res() res ) {
    const carts = await this.cartService.getCarts(); 
    return res.status(HttpStatus.OK).json({
      message: 'all carts ',
      carts: carts // = carts
    });
  }

  @Get('/:cartID')
  async getCart(@Res()res, @Param('cartID') cartID) {
    const cartGet = await this.cartService.getCart(cartID); 
    if(!cartGet) throw new NotFoundException('the cart not exists');
    return res.status(HttpStatus.OK).json({
      message: 'cart selected',
      cartGet: cartGet
    });
  }
  
  @Post('/create')
  async createCart(@Res() res, @Body() createCartDTO: CreateCartDTO) {
    const cart = await this.cartService.createCart(createCartDTO);
    return res.status(HttpStatus.OK).json({
      message: 'cart create successful',
      cart: cart
    });
  }

  @Put('/update')
  async updateCart(@Res() res, @Query('cartID') cartID, @Body() createCartDTO: CreateCartDTO) {
    const updateCart = await this.cartService.updateCart(cartID, createCartDTO);
    if(!updateCart) throw new NotFoundException('the cart not exists');
    return res.status(HttpStatus.OK).json({
      Message: 'cart actualized',
      updateCart: updateCart});
  }
  
  @Delete('/delete')
  async deleteCart(@Res() res, @Query('cartID') cartID) {
    const cartDeleted = await this.cartService.deleteCart(cartID);
    if(!cartDeleted) throw new NotFoundException('the cart not exists');
    return res.status(HttpStatus.OK).json({
      Message: 'cart deleted',
      cartDeleted: cartDeleted});
  }

}
