import { Injectable } from '@nestjs/common';
import { Model } from "mongoose";
import { InjectModel } from "@nestjs/mongoose";
import { cart } from "./interfaces/cart.interface";
import { CreateCartDTO } from "./dto/cart.dto";
import { create } from 'domain';

@Injectable()
export class CartService {

    constructor(@InjectModel('cart') private readonly cartModel: Model<cart> ) {}

        async getCarts(): Promise<cart[]> {
            const carts = await this.cartModel.find();
            return carts;
        }

        async getCart(cartID: String): Promise<cart> {
            const cart = await this.cartModel.findById(cartID);
            return cart;

        }

        async createCart(createCartDTO: CreateCartDTO): Promise<cart> {
            const cart =  new this.cartModel(createCartDTO);
            return await cart.save();
            //return cart;

        }

        async deleteCart(cartID: String): Promise<cart> {
            const deletedCart = await this.cartModel.findByIdAndDelete (cartID);
            return deletedCart;

        }

        async updateCart(cartID: String, createCartDTO: CreateCartDTO): Promise<cart> {
            const updateCart = await this.cartModel.findByIdAndUpdate(cartID, createCartDTO, {new: true});
            return updateCart;
        }
        
    
}
