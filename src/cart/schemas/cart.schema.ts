import { Schema } from "mongoose"

export const CartsSchema = new Schema({
    
    idShop: {type: String, required: true},
    idUser: {type: String, required: true},
    idProduct: {type: String, required: true},
    amount: Number,
    createAt: {
        type: Date,
        default: Date.now
    }
});