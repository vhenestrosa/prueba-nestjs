import { Module } from '@nestjs/common';
import { CartController } from './cart.controller';
import { CartService } from './cart.service';
import { MongooseModule, Schema } from "@nestjs/mongoose";
import { CartsSchema } from './schemas/cart.schema';

@Module({
  imports: [
    MongooseModule.forFeature([
      {name: 'cart', schema: CartsSchema} 
    ])
  ],
  controllers: [CartController],
  providers: [CartService],
})
export class CartModule {}
