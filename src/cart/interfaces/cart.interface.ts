import { Document } from "mongoose";

export interface cart extends Document {
    readonly idShop: String;
    readonly idUser: String;
    readonly idProduct: String;
    readonly amount: Number;
    readonly createAt: Date;
}