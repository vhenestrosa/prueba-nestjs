export class CreateCartDTO {

    readonly idShop: string;
    readonly idUser: string;
    readonly idProduct: string;
    readonly amount: number;
    
}