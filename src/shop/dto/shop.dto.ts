export class CreateShopDTO {

    readonly name: String;
    readonly description: String;
    readonly imageURL: String;
    readonly price: Number;
    readonly createAt: Date;
}