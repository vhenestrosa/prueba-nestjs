import { Controller, Get, Post, Put, Delete, Res, HttpStatus, Body, Param, NotFoundException, Query } from '@nestjs/common';

import { CreateShopDTO } from "./dto/shop.dto"; 
import { ShopService } from "./shop.service";

@Controller('shop')
export class ShopController {

  constructor(private shopService: ShopService) {}

  @Get('/')
  async getShops(@Res() res ) {
    const shops = await this.shopService.getShops(); 
    return res.status(HttpStatus.OK).json({
      message: 'all Shops ',
      shops: shops // = shops
    });
  }

  @Get('/:shopID')
  async getShop(@Res()res, @Param('shopID') shopID) {
    const shopGet = await this.shopService.getShop(shopID); 
    if(!shopGet) throw new NotFoundException('the shop not exists');
    return res.status(HttpStatus.OK).json({
      message: 'Shop selected',
      shopGet: shopGet
    });
  }
  
  @Post('/create')
  async createPost(@Res() res, @Body() createShopDTO: CreateShopDTO) {
    const shop = await this.shopService.createShop(createShopDTO);
    return res.status(HttpStatus.OK).json({
      message: 'Shop create successful',
      shop: shop
    });
  }

  @Put('/update')
  async updateShop(@Res() res, @Query('shopID') shopID, @Body() createShopDTO: CreateShopDTO) {
    const updateShop = await this.shopService.updateShop(shopID, createShopDTO);
    if(!updateShop) throw new NotFoundException('the shop not exists');
    return res.status(HttpStatus.OK).json({
      Message: 'Shop actualized',
      updateShop: updateShop});
  }
  
  @Delete('/delete')
  async deleteShop(@Res() res, @Query('shopID') shopID) {
    const shopDeleted = await this.shopService.deleteShop(shopID);
    if(!shopDeleted) throw new NotFoundException('the shop not exists');
    return res.status(HttpStatus.OK).json({
      Message: 'Shop deleted',
      shopDeleted: shopDeleted});
  }

}
