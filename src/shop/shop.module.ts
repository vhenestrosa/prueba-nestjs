import { Module } from '@nestjs/common';
import { ShopController } from './shop.controller';
import { ShopService } from './shop.service';
import { MongooseModule, Schema } from "@nestjs/mongoose";
import { ShopsSchema } from './schemas/shop.schema';

@Module({
  imports: [
    MongooseModule.forFeature([
      {name: 'Shop', schema: ShopsSchema} 
    ])
  ],
  controllers: [ShopController],
  providers: [ShopService],
})
export class ShopModule {}
