import { Injectable } from '@nestjs/common';
import { Model } from "mongoose";
import { InjectModel } from "@nestjs/mongoose";
import { Shop } from "./interfaces/shop.interface";
import { CreateShopDTO } from "./dto/shop.dto";
import { create } from 'domain';

@Injectable()
export class ShopService {

    constructor(@InjectModel('Shop') private readonly shopModel: Model<Shop> ) {}

        async getShops(): Promise<Shop[]> {
            const shops = await this.shopModel.find();
            return shops;
        }

        async getShop(shopID: String): Promise<Shop> {
            const shop = await this.shopModel.findById(shopID);
            return shop;

        }

        async createShop(createShopDTO: CreateShopDTO): Promise<Shop> {
            const shop =  new this.shopModel(createShopDTO);
            return await shop.save();
            //return shop;

        }

        async deleteShop(shopID: String): Promise<Shop> {
            const deletedShop = await this.shopModel.findByIdAndDelete (shopID);
            return deletedShop;

        }

        async updateShop(shopID: String, createShopDTO: CreateShopDTO): Promise<Shop> {
            const updateShop = await this.shopModel.findByIdAndUpdate(shopID, createShopDTO, {new: true});
            return updateShop;
        }
        
    
}
